# [![OxygenMC header](https://media.discordapp.net/attachments/794737458104893480/820000019364577280/banner.png)](#)
# OxygenMC

OxygenMC is a tool that allows you to run your own Minecraft server using openjdk 11 or newer, and aikars flags, even if your hosting doesn't support these! it also features auto updateing for your server software


## How does OxygenMC work?
OxygenMC will download and use a portable version of openjdk 11 or whatever is set in the config file, (unless you are already running openjdk 11 or newer) and the latest stable build of Yatopia, or what is set in the config file, and use Aikars flags to execute your Minecraft Server, this provides maximum performance and makes sure you are always secure with the latest updates!

OxygenMC is compiled and built using openjdk 8 therefore there should be no problems running it!

## Installation & usage

**Note: if you're trying to run this on hosting that uses pterodactyl or any other panel you probably don't need to do the steps described below but instead you should rename the OxygenMCsG jar file to server.jar if you don't have the option to change the startup file name in startup parameters**

if you don't want to build your own version from source you can just download one of our prebuilt releases by clicking [here](https://gitlab.com/a8_/OxygenMC/-/releases)

all you need to do now is to execute the jar file that you just downloaded and the rest will be taken care of
```bash
java -jar OxygenMC.jar
```

## Configuration
The config file should be self explainatory, but there is one thing I would like to make clear here!
the software options that are currently avalable for the software= property is as follows:
yatopia, papermc, purpur, tunity, airplane (note: tunity and airplane doesnt have such an API where we can specify minecraft version so it will always be latest mc ver they support)

note: graalvm doesnt have a proper DL api so it will always be latest ver of graalvm CE 


## Building
On Unix-like systems building should be straightforward all you need to do is just to run the gradlew build command in projects root directory.
```bash
./gradlew build
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)

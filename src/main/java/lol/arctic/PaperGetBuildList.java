package lol.arctic;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

public class PaperGetBuildList {
    OkHttpClient client = new OkHttpClient();

    JSONObject run(String version) throws IOException {
        Request request = new Request.Builder()
                .url("https://papermc.io/api//v2/projects/paper/versions/" + version)
                .build();

        Response response = client.newCall(request).execute();
        JSONObject jsonObject  = new JSONObject(Objects.requireNonNull(response.body()).string());
        return jsonObject;


}
}


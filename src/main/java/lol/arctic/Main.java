package lol.arctic;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) throws IOException {

        /*
        changelog from v1.5:
        added hyalus support (hyalus.io)
         */

        //declare it up here for scope reasons
        String pathToJava;
        String jdk = null;
        String software = null;
        String api = null;
        String version = null;
        String ramProperty = null;
        int ram;
        String jvm = null;



        System.out.println("OxygenMC v1.5 By Kamey and Arctic5824");

        //Load token from config
        File configFile = new File("OxygenMC.config");

        try {
            FileReader reader = new FileReader(configFile);
            Properties properties = new Properties();
            properties.load(reader);
            jdk = properties.getProperty("jdk");
            software = properties.getProperty("software");
            version = properties.getProperty("version");
            ramProperty = properties.getProperty("ram");
            jvm = properties.getProperty("jvm");
            reader.close();
        } catch (FileNotFoundException ex) {

            //stop program from trying to run

            //if file does not exist we will create file

            //if below is not "true" it means there was an warning

            if (!configFile.createNewFile()) {
                System.out.println("There was an issue creating the " + configFile.getName() + " file");
            } else {
                System.out.println("Created new config file" + configFile.getAbsolutePath());
            }
            //we are now writing default configuration to OxygenMC.config file

            FileWriter myWriter = new FileWriter(String.valueOf(configFile));
            myWriter.write("#OxygenMC configuration file");

            //windows handles new lines differently so we have java to choose what to do for new line, we dont need this here but its good practace

            myWriter.write(System.lineSeparator());
            myWriter.write(System.lineSeparator());

            //this writes to the files otherwise if we did another write it would overwrite first one
            myWriter.flush();

            myWriter.write("#java version e.g. jdk=11 or jdk=15");
            myWriter.write(System.lineSeparator());
            myWriter.write("jdk=11");
            myWriter.write(System.lineSeparator());
            myWriter.write(System.lineSeparator());
            myWriter.write("#software e.g. yatopia, yatopia-devel, papermc or hyalus");
            myWriter.write(System.lineSeparator());
            myWriter.write("software=yatopia");
            myWriter.write(System.lineSeparator());
            myWriter.write(System.lineSeparator());
            myWriter.write("#version e.g. 1.16.5 or 1.17");
            myWriter.write(System.lineSeparator());
            myWriter.write("version=1.16.5");
            myWriter.write(System.lineSeparator());
            myWriter.write(System.lineSeparator());
            myWriter.write("#ram in kb, e.g. 6000000 for 6gb or just use \"auto\" ");
            myWriter.write(System.lineSeparator());
            myWriter.write("ram=auto");
            myWriter.write(System.lineSeparator());
            myWriter.write(System.lineSeparator());
            myWriter.write("#the JVM to use, Hotspot, graal, or openj9, if you dont know what you want to use just stick with Hotspot!");
            myWriter.write(System.lineSeparator());
            myWriter.write("jvm=hotspot");
            myWriter.close();

            //okay lets load default config options now
            FileReader reader = new FileReader(configFile);
            Properties properties = new Properties();
            properties.load(reader);
            jdk = properties.getProperty("jdk");
            software = properties.getProperty("software");
            version = properties.getProperty("version");
            ramProperty = properties.getProperty("ram");
            jvm = properties.getProperty("jvm");
            reader.close();


        } catch (IOException IOError) {
            System.out.println("I/O error has occurred.");
            IOError.printStackTrace();

        }

        //to avoid issues
        jvm = jvm.toLowerCase(Locale.ROOT);
        software = software.toLowerCase(Locale.ROOT);


        //choose correct api for selected software

        switch (Objects.requireNonNull(software)) {
            case "yatopia":
                api = ("https://api.yatopiamc.org/v2/stableBuild/download?branch=ver/" + version);
                break;
            case "papermc":
                //we need to parse json to get the latest build number, I wish paper API had a better/easer way to do this, the old v1 (now deprecated) ver had basically what yatopia has

                PaperGetBuildList paperGetBuildList = new PaperGetBuildList();
                JSONObject responce = paperGetBuildList.run(version);


                String[] cleanedResponceString =
                        (responce.get("builds").
                                toString().
                                substring(1).
                                substring(0, responce.get("builds").toString().length() - 2).split(",")
                        );

                //now we must make cleanedResponceString an int
                int[] cleanedResponce = Stream.of(cleanedResponceString)
                        .mapToInt(Integer::parseInt).toArray();
                //now we find max value

                int buildToUse = Arrays.stream(cleanedResponce).max().getAsInt();



                // its time to make api string
                api = ("https://papermc.io/api/v2/projects/paper/versions/" + version + "/builds/" + buildToUse + "/downloads/paper-" + version + "-" + buildToUse + ".jar");
                break;
            case "purpur":
                api = ("https://purpur.pl3x.net/api/v1/purpur/" + version + "/latest/download");
                break;
            case "tuinity":
                api = ("https://ci.codemc.io/job/Spottedleaf/job/Tuinity/lastSuccessfulBuild/artifact/tuinity-paperclip.jar");
                break;
            case "airplane":
                api = ("https://dl.airplane.gg/latest/Airplane-JDK11/launcher-airplane.jar");
                break;

            case "yatopia-devel":
                api = ("https://api.yatopiamc.org/v2/latestBuild/download?branch=ver/" + version);
                break;
            case "hyalus":
                final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();


                Request request = new Request.Builder()
                        .url("https://www.gardling.com/jenkins/job/Hyalus/api/xml?xpath=/*/lastSuccessfulBuild/number")
                        .build();

                try (Response response = okHttpClient.newCall(request).execute()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                   String number = response.body().string().replace("<number>", "").replace("</number>", "");
                    System.out.println(number);
                   api = ("https://www.gardling.com/jenkins/job/Hyalus/lastSuccessfulBuild/artifact/target/hyalus-paperclip-b" + number + ".jar");

                }

                break;
            default:
                System.out.println("un-recognized option in config for software");
                break;
        }




        //TODO: auto eula... if its legal



        while (true) {
            //why is all this code in loop? because if server crashes it wil auto restart :D




            // if ramProperty doesn't equal to "auto" then this part of code will get the available system ram and use.
            if (ramProperty.equals("auto")) {
                long ramBytes = Runtime.getRuntime().maxMemory() - 1000000000;
                ram = (int) (ramBytes / 1024);
                System.out.println("running with " + ram + "kb" + " this should be 1GB less then your host gives you");
            }else{
                ram = Integer.parseInt(ramProperty);
                System.out.println("running with " + ram + "kb");
            }




            //check if server.jar exists and delete it if it does, we will download again it to make sure its on the latest version
            File serverJar = new File(System.getProperty("user.dir") + "/server1.jar");
            if (serverJar.exists()) {
                //we make sure delete goes well
                if (!(serverJar.delete())) {
                    System.out.println("there was an error deleting the server.jar file. please retry, if this issue persists try deleting the file manually, if this issue still persists contact, Arctic#5824 or Kamey#3257 on discord or send us an email at nikitadd22@gmail.com or tesla.vm123@gmail.com");
                }
            }



            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();


            Request request = new Request.Builder()
                    .url(Objects.requireNonNull(api))
                    .build();

            try (Response response = okHttpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                byte[] bytes = Objects.requireNonNull(response.body()).bytes();
                OutputStream writeJar = new FileOutputStream(serverJar);
                writeJar.write(bytes);
                writeJar.close();
            }

            //check if java11 or newer is being used to run our program then do proper thing to do

            System.out.println("a portable version of java " + jdk + " will be automatically downloaded and used, please wait a few seconds");

            //make sure the folder for java does not exist yet!
            File javaInstallFolder = new File(System.getProperty("user.dir") + "/javaInstall");
            File javaDownloadFile = new File(javaInstallFolder + "/java.tar.gz");
            File javaExtractedFolder = new File(javaInstallFolder + "/java");

            if (javaInstallFolder.exists()) {
                //using apache fileutils to delete the dir, might not be worth adding apache file utils to project just for this but i can rewrite doing this if needed
                FileUtils.deleteDirectory(javaInstallFolder);
            }

            //   alright lets get java

            //   make the folder to extract java to

            boolean bool = javaInstallFolder.mkdir();
            //if it fails to make a folder, bool returns true if there is no issues, so if bool is not true there is issue
            if (!bool) {
                System.out.println("Sorry couldn't create specified directory");
                exit(2);
            }

            //okay we have created the folder now
            //lets download proper version of java
            if(jvm.equals("hotspot") || jvm.equals("openj9")) {
                InputStream inputStream = new URL("https://api.adoptopenjdk.net/v3/binary/latest/" + jdk + "/ga/linux/x64/jdk/" + jvm + "/normal/adoptopenjdk?project=jdk").openStream();
                Files.copy(inputStream, Paths.get(String.valueOf(javaDownloadFile)), StandardCopyOption.REPLACE_EXISTING);

            }else if(jvm.equals("graal")){
                //TODO: test
                request = new Request.Builder()
                        .url("https://glare.now.sh/graalvm/graalvm-ce-builds/graalvm-ce-java11-linux-amd64-.*.tar.gz")
                        .build();

                try (Response response = okHttpClient.newCall(request).execute()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    byte[] bytes = Objects.requireNonNull(response.body()).bytes();
                    OutputStream writeJar = new FileOutputStream(javaDownloadFile);
                    writeJar.write(bytes);
                    writeJar.close();
                }
            }

            //cool now we have java downloaded, lets extract"

            Archiver archiver = ArchiverFactory.createArchiver("tar", "gz");
            //first input is archive, second is destination
            archiver.extract(javaDownloadFile, javaExtractedFolder);

            //okay extraction has finished now we can delete tar.gz file
            if (!(javaDownloadFile.delete())) {
                System.out.println("Sorry there was an error deleting java.tar.gz file");
            }

            //set java path to proper location


            //first we must get the jdk folder name as it will change with each java update.



            String[] javaFolderArray = javaExtractedFolder.list();
            assert javaFolderArray != null;
            String javaFolder = javaFolderArray[0];

            pathToJava = javaExtractedFolder + "/" + javaFolder + "/bin/";




            {
                //start server
                ProcessBuilder processBuilder = new ProcessBuilder();

                //makes sure that we can run java executable
                processBuilder.command("chmod", "+x " + pathToJava + "java");

                //each different parameters need to be separated via comma
                processBuilder.command(pathToJava + "java", "-Xms" + ram + "K", "-Xmx" + ram + "K", "-XX:+UseG1GC", "-XX:+ParallelRefProcEnabled", "-XX:MaxGCPauseMillis=200", "-XX:+UnlockExperimentalVMOptions", "-XX:+DisableExplicitGC", "-XX:+AlwaysPreTouch", "-XX:G1NewSizePercent=30", "-XX:G1MaxNewSizePercent=40", "-XX:G1HeapRegionSize=8M", "-XX:G1ReservePercent=20", "-XX:G1HeapWastePercent=5", "-XX:G1MixedGCCountTarget=4", "-XX:InitiatingHeapOccupancyPercent=15", "-XX:G1MixedGCLiveThresholdPercent=90", "-XX:G1RSetUpdatingPauseTimePercent=5", "-XX:SurvivorRatio=32", "-XX:+PerfDisableSharedMem", "-XX:MaxTenuringThreshold=1", "-Dusing.aikars.flags=https://mcflags.emc.gs", "-Daikars.new.flags=true",  "--illegal-access=warn", "-jar", String.valueOf(serverJar), "nogui");

                try {


                    processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
                    processBuilder.redirectInput(ProcessBuilder.Redirect.INHERIT);
                    Process process = processBuilder.start();


                    int exitCode = process.waitFor();

                    if (exitCode != 0) {
                        System.out.println("Server exited with exit code " + exitCode);
                        break;
                    } else {
                        System.out.println("Server closed");
                    }


                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }

            }


        }
    }


}
